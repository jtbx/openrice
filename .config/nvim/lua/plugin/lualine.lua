-- lualine configuration and theme
-- imitates vim-airline theme 'jellybeans'

local colors = {
	red   = '#870000',
	blue  = '#005faf',
	green = '#005f00',
	cyan  = '#00afaf',
	black = '#080808',
	white = '#ffffff',
	grey  = '#303030',
}

local config = {
	options = {
	    icons_enabled = true,
	    theme = {
	    	normal = {
	    		a = { fg = colors.white, bg = colors.blue, gui = 'bold' },
	    		b = { fg = colors.white, bg = colors.grey },
	    		c = { fg = colors.white, bg = colors.black },
	    	},

	    	insert = { a = { fg = colors.white, bg = colors.green, gui = 'bold' } },
	    	visual = { a = { fg = colors.white, bg = colors.red, gui = 'bold' } },
	    	replace = { a = { fg = colors.white, bg = colors.cyan, gui = 'bold' } },

	    	inactive = {
	    		a = { fg = colors.white, bg = colors.black },
	    		b = { fg = colors.white, bg = colors.black },
	    		c = { fg = colors.black, bg = colors.black },
	    	},
	    },
	   	component_separators = { left = '', right = ''},
	   	section_separators = { left = '', right = ''},
	},
	sections = {
		lualine_a = {
			{ 'mode', separator = { left = '' }, right_padding = 2 },
		},
		lualine_b = { 'filename', { 'branch', icon = {'שׂ'}, }},
		lualine_c = {},
		lualine_x = {},
		lualine_y = { 'filetype', 'progress' },
		lualine_z = {
			{ 'location', separator = { right = '' }, left_padding = 2 },
		},
	},
	inactive_sections = {
		lualine_a = { 'filename' },
		lualine_b = {},
		lualine_c = {},
		lualine_x = {},
		lualine_y = {},
		lualine_z = { 'location' },
	},
	tabline = {},
	extensions = {},
}

require('lualine').setup(config)
