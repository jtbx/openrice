-- Decrease update time
o.timeoutlen = 500
o.updatetime = 300

-- Number of screen lines to keep above and below the cursor
o.scrolloff = 8

-- Better editor UI
o.background = 'dark'
o.number = true
o.numberwidth = 5
o.relativenumber = true
o.signcolumn = 'yes:2'
o.cursorline = true

-- Better editing experience
o.expandtab = true
o.cindent = true
o.autoindent = true
o.wrap = true
o.textwidth = 300
o.tabstop = 4
o.shiftwidth = 4
o.softtabstop = -1 -- If negative, shiftwidth value is used
o.completeopt = "menu,menuone,preview"

-- Appearance
cmd('colorscheme delek')
-- Don't show mode hints (because lualine is better)
cmd('set noshowmode') -- can't set it w/ vim.o
api.nvim_set_hl(0, 'Pmenu', {ctermfg=25, ctermbg=235})
api.nvim_set_hl(0, 'PmenuSel', {ctermfg=235, ctermbg=26})
api.nvim_set_hl(0, 'Search', {ctermfg=15, ctermbg=25})
api.nvim_set_hl(0, 'LineNr', {ctermfg=25, ctermbg=235})
api.nvim_set_hl(0, 'CursorLine', {ctermbg=235})
api.nvim_set_hl(0, 'CursorColumn', {ctermbg=235})

-- Case insensitive searching UNLESS /C or capital in search
o.ignorecase = true
o.smartcase = true

-- Undo and backup options
o.backup = false
o.writebackup = false
o.undofile = true
o.swapfile = false

-- Remember 50 items in commandline history
o.history = 50

-- Better buffer splitting
o.splitright = true
o.splitbelow = true
