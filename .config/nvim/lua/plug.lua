require 'packer'

return require('packer').startup(function(use)
	use 'wbthomason/packer.nvim'
	use 'flazz/vim-colorschemes'
	use {
		'nvim-lualine/lualine.nvim',
		requires = { 'kyazdani42/nvim-web-devicons', opt = true },
		config = { require 'plugin/lualine' }
	}
	use 'ap/vim-css-color'
end)
