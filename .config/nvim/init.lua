-- Jeremy's Neovim Lua configuration
-- Based on nvimdots

g = vim.g
o = vim.o
api = vim.api
cmd = vim.cmd

require 'pref'
require 'bind'
require 'plug'
