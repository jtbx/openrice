# ricepuffs makefile
# dwm, st, dmenu, and nsxiv targets all build and install
# other targets install configuration files

PREFIX = /usr/local

install: dwm st dmenu nsxiv xsession neovim neofetch
	@echo "You may want to customise the source code of dwm, st, dmenu, and nsxiv."
	@echo "Their source code has been copied to .config, so to customise dwm's source code for example, you would enter into ~/.config/dwm."

dwm:
	make -C .config/dwm -f Makefile clean install
	mkdir -p ~/.config
	cp -R .config/dwm ~/.config
st:
	make -C .config/st -f Makefile clean install
	mkdir -p ~/.config
	cp -R .config/st ~/.config
dmenu:
	make -C .config/dmenu -f Makefile clean install
	mkdir -p ~/.config
	cp -R .config/dmenu ~/.config
nsxiv:
	make -C .config/nsxiv -f Makefile clean install
	mkdir -p ~/.config
	cp -R .config/nsxiv ~/.config

xsession:
	cp .xsession ~/.xsession
neovim: # requires vim-plug
	mkdir -p ~/.config/nvim
	cp -R .config/nvim/* ~/.config/nvim
packer:
	@echo "Installing packer for Neovim..."
	git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim
neofetch:
	mkdir -p ~/.config/neofetch
	cp .config/neofetch/config.conf ~/.config/neofetch/config.conf
